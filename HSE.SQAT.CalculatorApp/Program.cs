﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSE.SQAT.CalculatorApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Arrange.
            var value1 = 3;
            var value2 = 2;
            var value3 = 2;
            // Act.
            var calculator = new Calculator();
            calculator.PressDisplay(value1);
            calculator.PressMinus();
            calculator.PressDisplay(value2);
            calculator.PressMinus();
            calculator.PressDisplay(value3);
            calculator.PressEnter();
            var actual = calculator.Display;
            // Assert.
            var expected = -1;
            Console.WriteLine(actual);
            Console.ReadKey();
        }
    }
}
