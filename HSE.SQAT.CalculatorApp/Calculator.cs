﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSE.SQAT.CalculatorApp
{
    public partial class Calculator
    {
        double Result
        {
            get
            {
                double res = values[0];

                for (int i = 0; i < this.operators.Count; i++)
                { 
                    if (i == this.values.Count-1)
                        return res;
                        res = calcValues(res, this.values[i + 1], this.operators[i]);
                }
                return res;
            }
        }
        List<Operator> operators;
        List<double> values;

        public double Display => Result;
        public Calculator()
        {
            this.operators = new List<Operator>();
            this.values = new List<double>();
        }
        public void PressEnter()
        {

        }
        public void PressDisplay(double value) => this.values.Add(value);
        public void PressPlus() => this.operators.Add(Operator.Plus);
        public void PressMinus() => this.operators.Add(Operator.Minus);
        public void PressMultiply() => this.operators.Add(Operator.Multiply);
        public void PressDivide() => this.operators.Add(Operator.Divide);

        double calcValues(double a,double b, Operator @operator)
        {
            
            switch (@operator)
            {
                case Operator.Divide:
                    if(a==0 || b == 0)
                        throw new DivideByZeroException();
                    return a / b;
                case Operator.Minus:
                   
                    return a - b;
                case Operator.Multiply:
                    return a * b;
                case Operator.Plus:
                    return a + b;
                default:
                    return 0;
            }
        }
    }
}
