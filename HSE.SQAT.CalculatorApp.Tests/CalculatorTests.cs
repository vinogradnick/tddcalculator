﻿using System;
using System.Linq;
using NUnit.Framework;

namespace HSE.SQAT.CalculatorApp.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator calculator;

        [SetUp]
        public void Setup() => this.calculator = new Calculator();

        [TestCase(1,2,
            Author ="Vinogradov Nikita",
            TestName ="Сложение 1+2",
            Category ="Ввод значений по порядку",
             ExpectedResult = 3.0
            )]
        [TestCase(1, 3,
            
            Author = "Vinogradov Nikita",
            TestName = "Сложение 1+3",
            Category = "Ввод значений по порядку",
            ExpectedResult =4.0
            )]
        [TestCase(null,null,
            Author = "Vinogradov Nikita",
            TestName = "Сложение 1+3",
            Category = "Ввод значений по порядку",
            ExpectedResult = null
            )]
        [TestCase(0, 0,
            Author = "Vinogradov Nikita",
            TestName = "Сложение нулей",
            Category = "Ввод значений по порядку",
            ExpectedResult =0
            )]
        public double PlusTest(params double[] inputValues)
        {
            for (int i = 0; i < inputValues.Length; i++)
            {
                this.calculator.PressDisplay(inputValues[i]);
                this.calculator.PressPlus();
            }
            return this.calculator.Display;
        }
        [TestCase(1, 2,
          Author = "Vinogradov Nikita",
          TestName = "Вычитание 1-2",
          Category = "Ввод значений по порядку",
           ExpectedResult = -1.0
          )]
        public double  MinusTest(params double[] inputValues)
        {

          
      
            for (int i = 0; i < inputValues.Length; i++)
            {
               
                this.calculator.PressDisplay(inputValues[i]);
                this.calculator.PressMinus();
            }
            return this.calculator.Display;
        }



        [TestCase(1, 2,
            Author = "Vinogradov Nikita",
            TestName = "Деление 1 / 2",
            Category = "Ввод значений по порядку",
            ExpectedResult =0.5
            )]
        public double DivideTest(params double[] inputValues)
        {

            for (int i = 0; i < inputValues.Length; i++)
            {
                this.calculator.PressDisplay(inputValues[i]);
                this.calculator.PressDivide();

            }
            return this.calculator.Display;


        }
        [TestCase(1, 2,
          Author = "Vinogradov Nikita",
          TestName = "Умножение 1 * 2",
          Category = "Ввод значений по порядку",
            ExpectedResult =2.0
          )]
        public double MultiplyTest(params double[] inputValues)
        {

            
            for (int i = 0; i < inputValues.Length; i++)
            {
                this.calculator.PressDisplay(inputValues[i]);
                this.calculator.PressMultiply();
            }
            return this.calculator.Display;


        }

    }
}
